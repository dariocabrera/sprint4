const express = require("express");
const router = express.Router();
const passport = require("passport");
const Strategy_name = "linkedin";
require("dotenv").config()

router.get(`/${Strategy_name}/auth`, passport.authenticate( Strategy_name, {
    session:false, 
    scope: ['r_liteprofile','r_emailaddress'] }))

router.get(`/${Strategy_name}/callback`, 
  passport.authenticate(Strategy_name, {session:false, failureRedirect: '/failed' }),
  function(req, res) {
    console.log(`peticion get /${Strategy_name}/callback`)
    const data = req.user._json
    console.log("data:")
    console.log(data)

    const token = "hgjsd8fs6g7s7df67g6sdf43sdg2s3df5sg6s7df7"
    const url_front = process.env.URL_FRONT+`/?token=${token}`
    res.redirect(301,url_front)
  });
  module.exports = router