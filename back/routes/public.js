const express = require("express")
const router = express.Router()

router.get("/", function(req,res){
    console.log("peticion GET a /")
    res.send("HELLO WORLD")
})

router.get('/success', function(req,res){
    res.send('success');
})

router.get('/failure', function(req,res){
    res.send('failure')
})

router.get('/pending',function(req,res){
    res.send('pending')
})

module.exports = router